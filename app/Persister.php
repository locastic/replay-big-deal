<?php

class Persister
{
    /**
     * @var PDO $conn
     */
    private $conn;
    /**
     * @param PDO $conn
     */
    public function __construct(\PDO $conn)
    {
        $this->conn = $conn;
    }
    /**
     * @param array $data
     */
    public function persist($data)
    {
        if ($this->userExists($data['id'])) {
            return null;
        }

        $facebookId = $data['id'];
        $firstName = $data['first_name'];
        $lastName = $data['last_name'];
        $email = $data['email'];
        $birthday = $data['birthday'];
        $location = $data['location'];
        $gender = $data['gender'];
        $fullName = $firstName.' '.$lastName;

        $stmt = $this->conn->prepare('INSERT INTO users
                                          (facebook_id, first_name, last_name, email, birthday, location, gender, full_name)
                                      VALUES (:facebook_id, :first_name, :last_name, :email, :birthday, :location, :gender, :full_name)');

        $stmt->bindParam(':facebook_id', $facebookId);
        $stmt->bindParam(':first_name', $firstName);
        $stmt->bindParam(':last_name', $lastName);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':birthday', $birthday);
        $stmt->bindParam(':location', $location);
        $stmt->bindParam(':gender', $gender);
        $stmt->bindParam(':full_name', $fullName);

        $stmt->execute();
    }

    private function userExists($facebookId)
    {
        $stmt = $this->conn->prepare('SELECT id FROM users WHERE facebook_id = :facebook_id');

        $stmt->bindParam(':facebook_id', $facebookId);

        $stmt->execute();

        $data = $stmt->fetchAll();

        if (isset($data[0]['id'])) {
            return true;
        }

        return false;
    }
}